package com.epam

import java.net.URI

import org.apache.hadoop.conf.Configuration
import org.apache.hadoop.fs.{FileSystem, Path}
import org.apache.spark.sql.{SaveMode, SparkSession}

import scala.collection.JavaConversions._

object Compact extends App {

  override def main(args: Array[String]): Unit = {

    val inputPath = args(0)

    val spark = SparkSession
      .builder()
      .appName("Spark Compaction")
      .getOrCreate()

    val conf = new Configuration()
    conf.addResource(new Path("file:///etc/hadoop/conf/core-site.xml"));
    conf.addResource(new Path("file:///etc/hadoop/conf/hdfs-site.xml"));
    conf.set("fs.hdfs.impl", classOf[org.apache.hadoop.hdfs.DistributedFileSystem].getName)
    val fileSystem = FileSystem.get(URI.create(inputPath), conf)

    val partitionsList = getPartitionPathList(fileSystem, new Path(inputPath))
    val partitionsListFiltered = partitionsList.filter(p => countNumberOfFiles(conf, p) > 2)

    for (p <- 0 to (partitionsListFiltered.size - 2)) {
      val path = partitionsListFiltered.get(p)
      val avroFiles = spark
        .read
        .format("com.databricks.spark.avro")
        .load(path.toString)

      val tmpOutputPath = path.toString + "_tmp"
      avroFiles
        .coalesce(1)
        .write
        .mode(SaveMode.Overwrite)
        .format("com.databricks.spark.avro")
        .save(tmpOutputPath)

      fileSystem.delete(path, true)
      fileSystem.rename(new Path(tmpOutputPath), path)
      spark.close()
    }
  }

  import java.util

  @throws[Exception]
  protected def getPartitionPathList(fs: FileSystem, path: Path): util.ArrayList[Path] = {
    val files = fs.listStatus(path)
    val paths = new util.ArrayList[Path]
    if (files != null) for (eachFile <- files) {
      if (eachFile.isFile) {
        paths.add(path)
        return paths
      }
      else paths.addAll(getPartitionPathList(fs, eachFile.getPath))
    }
    paths
  }

  protected def countNumberOfFiles(conf: Configuration, path: Path): Int = {
    val fileSystem = FileSystem.get(URI.create(path.toString), conf)
    var numberFiles = 0
    val status = fileSystem.listStatus(path)
    status
      .filter(d => d.isFile)
      .foreach(_ => numberFiles = numberFiles + 1)
    numberFiles
  }

}
