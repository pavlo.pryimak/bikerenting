select
  end_day_of_the_week,
  end_station_meteorological_events,
  sum(duration) as total_trips_duration
from trips_with_end_weather
group by
  end_station_meteorological_events,
  end_day_of_the_week
order by
  sum(duration) desc;


