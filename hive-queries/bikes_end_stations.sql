/* Add end_station_zipcode on which to join with weather */
create temporary table if not exists trips_with_end_zipcode
as select 
  trips_converted_dates.id,
  trips_converted_dates.duration,
  trips_converted_dates.start_date,
  trips_converted_dates.start_station_name,
  trips_converted_dates.start_station_id,
  stations_with_zipcodes.zipcode as end_station_zipcode,
  trips_converted_dates.end_date,
  trips_converted_dates.end_station_name,
  trips_converted_dates.end_station_id,
  trips_converted_dates.bike_id,
  trips_converted_dates.subscription_type
from trips_converted_dates join stations_with_zipcodes
on trips_converted_dates.end_station_id = stations_with_zipcodes.id;


/* Create the table to run queries against */
create table if not exists trips_with_end_weather
as select
  trips_with_station_zipcodes.*,
  date_format(trips_with_station_zipcodes.end_date, 'u') as end_day_of_the_week,
  weather_converted.max_temperature_c as end_station_max_temperature_c,
  weather_converted.mean_temperature_c as end_station_mean_temperature_c,
  weather_converted.min_temperature_c as end_station_min_temperature_c,
  weather_converted.max_dew_point_c as end_station_max_dew_point_c,
  weather_converted.mean_dew_point_c as end_station_mean_dew_point_c,
  weather_converted.min_dew_point_c as end_station_min_dew_point_c,
  weather_converted.max_humidity as end_station_max_humidity,
  weather_converted.mean_humidity as end_station_mean_humidity,
  weather_converted.min_humidity as end_station_min_humidity,
  weather_converted.max_sea_level_pressure_mm as end_station_max_sea_level_pressure_mm,
  weather_converted.mean_sea_level_pressure_mm as end_station_mean_sea_level_pressure_mm,
  weather_converted.min_sea_level_pressure_mm as end_station_min_sea_level_pressure_mm,
  weather_converted.max_visibility_km as end_station_max_visibility_km,
  weather_converted.mean_visibility_km as end_station_mean_visibility_km,
  weather_converted.min_visibility_km as end_station_min_visibility_km,
  weather_converted.max_wind_speed_kmph as end_station_max_wind_speed_kmph,
  weather_converted.mean_wind_speed_kmph as end_station_mean_wind_speed_kmph,
  weather_converted.max_gust_speed_kmph as end_station_max_gust_speed_kmph,
  weather_converted.precipitation_mm as end_station_precipitation_mm,
  weather_converted.cloud_cover as end_station_cloud_cover,
  weather_converted.meteorological_events as end_station_meteorological_events
from trips_with_station_zipcodes left join weather_converted
on trips_with_station_zipcodes.end_date = weather_converted.date_taken
and trips_with_station_zipcodes.end_station_zipcode = weather_converted.zip_code;


/* Clean up */
drop table trips_with_end_zipcode;


/* Run some queries */

