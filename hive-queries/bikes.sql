/* Create stations table from avro schema */
create temporary external table stations_schema
row format serde 'org.apache.hadoop.hive.serde2.avro.AvroSerDe'
stored as avro
tblproperties ('avro.schema.url'='hdfs:///bikes/station/.metadata/schema.avsc');

create external table if not exists stations
like stations_schema
stored as parquet
location "hdfs:///bikes/station"
;

drop table stations_schema;


/* Create trips table from avro schema */
create temporary external table if not exists trips_schema
row format serde 'org.apache.hadoop.hive.serde2.avro.AvroSerDe'
stored as avro
tblproperties ('avro.schema.url'='hdfs:///bikes/trip/.metadata/schema.avsc');

create external table if not exists trips
like trips_schema
stored as parquet
location "hdfs:///bikes/trip"
;

drop table trips_schema;



/* Transform start and end date to datetime objects */
create view if not exists trips_converted_dates
as select
  id,
  duration,
  to_date(from_unixtime(cast(start_date / 1000 as bigint))) as start_date,
  start_station_name,
  start_station_id,
  to_date(from_unixtime(cast(end_date / 1000 as bigint))) as end_date,
  end_station_name,
  end_station_id,
  bike_id,
  subscription_type
from trips;


/* Create weather table from avro schema */
create temporary external table if not exists weather_schema
row format serde 'org.apache.hadoop.hive.serde2.avro.AvroSerDe'
stored as avro
tblproperties ('avro.schema.url'='hdfs:///bikes/weather/.metadata/schema.avsc');

create external table if not exists weather
like weather_schema
stored as parquet
location "hdfs:///bikes/weather"
;

drop table weather_schema;


/* Extract date from date_taken to join with trip data later on */
/* Convert imperial units to metric */
/* Convert lowercase 'rain' to 'Rain' in events column */
/* Replace NULL in events column to 'None' */
create view if not exists weather_converted
as select 
  to_date(from_unixtime(cast(`date` / 1000 as bigint))) as date_taken,
  round(((max_temperature_f - 32) * 5 / 9), 1) as max_temperature_c,
  round(((mean_temperature_f - 32) * 5 / 9), 1) as mean_temperature_c,
  round(((min_temperature_f - 32) * 5 / 9), 1) as min_temperature_c,
  round(((max_dew_point_f - 32) * 5 / 9), 1) as max_dew_point_c,
  round(((mean_dew_point_f - 32) * 5 / 9), 1) as mean_dew_point_c,
  round(((min_dew_point_f - 32) * 5 / 9), 1) as min_dew_point_c,
  max_humidity,
  mean_humidity,
  min_humidity,
  round(max_sea_level_pressure_inches * 25.4) as max_sea_level_pressure_mm,
  round(mean_sea_level_pressure_inches * 25.4) as mean_sea_level_pressure_mm,
  round(min_sea_level_pressure_inches * 25.4) as min_sea_level_pressure_mm,
  round((max_visibility_miles * 1.609344), 1) as max_visibility_km,
  round((mean_visibility_miles * 1.609344), 1) as mean_visibility_km,
  round((min_visibility_miles * 1.609344), 1) as min_visibility_km,
  round((max_wind_speed_mph * 1.609344), 1) as max_wind_speed_kmph,
  round((mean_wind_speed_mph * 1.609344), 1) as mean_wind_speed_kmph,
  round((max_gust_speed_mph * 1.609344), 1) as max_gust_speed_kmph,
  round(precipitation_inches * 25.4) as precipitation_mm,
  cloud_cover,
  coalesce(regexp_replace(events, "rain", "Rain"), "None") as meteorological_events,
  wind_dir_degrees,
  zip_code 
from weather;


/* Create table for getting a station_zipcode from station_city */
create table zipcodes (city string, zipcode int);
insert into table zipcodes values ("San Francisco", 94107);
insert into table zipcodes values ("Redwood City", 94063);
insert into table zipcodes values ("Palo Alto", 94301);
insert into table zipcodes values ("Mountain View", 94041);
insert into table zipcodes values ("San Jose", 95113);


/* Maybe should be view */
create table if not exists stations_with_zipcodes
as
select stations.*, zipcodes.zipcode
from stations join zipcodes
on stations.city = zipcodes.city;


/* Add start_station_zipcode on which to join with weather */
create temporary table if not exists trips_with_start_zipcode
as select 
  trips_converted_dates.id,
  trips_converted_dates.duration,
  trips_converted_dates.start_date,
  trips_converted_dates.start_station_name,
  trips_converted_dates.start_station_id,
  stations_with_zipcodes.zipcode as start_station_zipcode,
  trips_converted_dates.end_date,
  trips_converted_dates.end_station_name,
  trips_converted_dates.end_station_id,
  trips_converted_dates.bike_id,
  trips_converted_dates.subscription_type
from trips_converted_dates join stations_with_zipcodes
on trips_converted_dates.start_station_id = stations_with_zipcodes.id;


/* Create the table to run queries against */
create table if not exists trips_with_start_weather
as select
  trips_with_station_zipcodes.*,
  date_format(trips_with_station_zipcodes.start_date, 'u') as start_day_of_the_week,
  weather_converted.max_temperature_c as start_station_max_temperature_c,
  weather_converted.mean_temperature_c as start_station_mean_temperature_c,
  weather_converted.min_temperature_c as start_station_min_temperature_c,
  weather_converted.max_dew_point_c as start_station_max_dew_point_c,
  weather_converted.mean_dew_point_c as start_station_mean_dew_point_c,
  weather_converted.min_dew_point_c as start_station_min_dew_point_c,
  weather_converted.max_humidity as start_station_max_humidity,
  weather_converted.mean_humidity as start_station_mean_humidity,
  weather_converted.min_humidity as start_station_min_humidity,
  weather_converted.max_sea_level_pressure_mm as start_station_max_sea_level_pressure_mm,
  weather_converted.mean_sea_level_pressure_mm as start_station_mean_sea_level_pressure_mm,
  weather_converted.min_sea_level_pressure_mm as start_station_min_sea_level_pressure_mm,
  weather_converted.max_visibility_km as start_station_max_visibility_km,
  weather_converted.mean_visibility_km as start_station_mean_visibility_km,
  weather_converted.min_visibility_km as start_station_min_visibility_km,
  weather_converted.max_wind_speed_kmph as start_station_max_wind_speed_kmph,
  weather_converted.mean_wind_speed_kmph as start_station_mean_wind_speed_kmph,
  weather_converted.max_gust_speed_kmph as start_station_max_gust_speed_kmph,
  weather_converted.precipitation_mm as start_station_precipitation_mm,
  weather_converted.cloud_cover as start_station_cloud_cover,
  weather_converted.meteorological_events as start_station_meteorological_events
from trips_with_station_zipcodes left join weather_converted
on trips_with_station_zipcodes.start_date = weather_converted.date_taken
and trips_with_station_zipcodes.start_station_zipcode = weather_converted.zip_code;


/* Clean up */
drop table trips_with_start_zipcode;


/* Run some queries */
select
  id, duration, start_date, subscription_type,
  start_station_mean_temperature_c, 
  start_station_precipitation_mm
from trips_with_start_weather
where start_station_meteorological_events = "Rain"
or start_station_meteorological_events = "Fog-Rain"
or start_station_meteorological_events = "Rain-Thunderstorm"
order by duration DESC
limit 100;


select
  id,
  duration,
  start_date,
  start_day_of_the_week,
  start_station_name,
  start_station_mean_temperature_c,
  start_station_meteorological_events,
  start_station_precipitation_mm
from trips_with_start_weather
where start_day_of_the_week > 5
and (start_station_meteorological_events = "Rain"
or start_station_meteorological_events = "Fog-Rain"
or start_station_meteorological_events = "Rain-Thunderstorm")
order by duration DESC
limit 10;



/* May be too much to do at once */
/*
create table if not exists trips_with_station_zipcodes
as select
  trips_with_start_zipcode.*,
  stations_with_zipcodes.zipcode as end_station_zipcode
from trips_with_start_zipcode join stations_with_zipcodes
on trips_with_start_zipcode.end_station_id = stations_with_zipcodes.id;

create table if not exists trips_with_start_weather
as select *
from trips_with_station_zipcodes join weather_converted_dates
*/


