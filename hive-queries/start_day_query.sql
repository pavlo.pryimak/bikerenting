select
  start_day_of_the_week,
  start_station_meteorological_events,
  count(start_day_of_the_week) as number_of_trips
from trips_with_start_weather
group by
  start_station_meteorological_events,
  start_day_of_the_week
order by
  count(start_day_of_the_week) desc,
  start_station_meteorological_events,
  start_day_of_the_week;



