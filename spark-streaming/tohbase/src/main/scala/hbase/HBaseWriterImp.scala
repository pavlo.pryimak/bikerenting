package hbase

import util.{Properties, StationStatusInfo}

class HBaseWriterImp(namespace: String,
                     tableNameSSI: String, familyNameSSI: String,
                     tableNameKE: String, familyNameKE: String)
    extends HBaseWriter {
    
    var batchList: List[StationStatusInfo] = List[StationStatusInfo]()
    val executor = new SchemaOperation(Properties.HBASE_CONFIG,Properties.NAMESPACE)
    executor.configOurTableInHBase(tableNameSSI, familyNameSSI)
    executor.configOurTableInHBase(tableNameKE, familyNameKE)
    val daoSDM = new StreamingDataModelDAO(namespace, tableNameSSI )
    val daoKE = new KafkaEventDAO(namespace, tableNameKE )
    
    
    override def writeStationStatusInfoToHBase(sdm: StationStatusInfo): StationStatusInfo = {
        batchList = batchList :+ sdm
        if(batchList.size > Properties.BUTCH_SIZE_TO_WRITE_HBASE){
            daoSDM.putBatchToTableStationStatus(batchList, Properties.FAMILY_NAME_SDM)
            batchList = List[StationStatusInfo]()
        }
        sdm
    }
    
    override def writeKafkaEventToHBase(kafkaMessage: (String,String)): (String,String) = {
        daoKE.putKafkaEventToHBase(kafkaMessage, Properties.FAMILY_NAME_KE)
        kafkaMessage
    }
    
    override def flushIfRemain(): Unit = {
        if(batchList.size > 0){
            daoSDM.putBatchToTableStationStatus(batchList, Properties.FAMILY_NAME_SDM)
        }
    }
}
