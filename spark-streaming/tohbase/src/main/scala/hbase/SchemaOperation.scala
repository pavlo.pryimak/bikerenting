package hbase

import org.apache.hadoop.conf.Configuration
import org.apache.hadoop.hbase.client.{ColumnFamilyDescriptorBuilder, ConnectionFactory, TableDescriptorBuilder}
import org.apache.hadoop.hbase.util.Bytes
import org.apache.hadoop.hbase.TableName

import scala.collection.JavaConversions._

class SchemaOperation(@transient val hbaseConfig: Configuration, namespase: String) extends Serializable {
    
    def createTable(tableName: String, family: String*): Unit = {
        TableName.isLegalFullyQualifiedTableName(Bytes.toBytes(tableName))
        if(family.size < 1){
            throw new IllegalArgumentException("Table must contain at least one columnFamily")
        }
        val connection = ConnectionFactory.createConnection(hbaseConfig)
        val admin = connection.getAdmin
        
        val name = TableName.valueOf(namespase + ":" + tableName)
        val tableBuilder = TableDescriptorBuilder.newBuilder(name)
        for (f <- family){
            tableBuilder.setColumnFamily(ColumnFamilyDescriptorBuilder.of(f))
        }
        val table =  tableBuilder.build()
        admin.createTable(table)
        connection.close()
    }
    
    def renameTable(oldName: String, newName: String): Unit = {
        TableName.isLegalFullyQualifiedTableName(Bytes.toBytes(newName))
        val connection = ConnectionFactory.createConnection(hbaseConfig)
        val admin = connection.getAdmin
        
        val oldTableName = TableName.valueOf(namespase + ":" + oldName)
        val newTableName = TableName.valueOf(namespase + ":" + newName)
        val snapshotName = "my_snapshot"
        
        if(admin.tableExists(oldTableName)){
            if(admin.isTableEnabled(oldTableName)){
                admin.disableTable(oldTableName)
            }
            admin.snapshot(snapshotName, oldTableName)
            admin.cloneSnapshot(snapshotName, newTableName)
            admin.deleteSnapshot(snapshotName)
            admin.deleteTable(oldTableName)
            if(admin.isTableDisabled(newTableName)){
                admin.enableTable(newTableName)
            }
        }
        
        connection.close()
    }
    
    def deleteTable(tableName: String): Unit = {
        val connection = ConnectionFactory.createConnection(hbaseConfig)
        val admin = connection.getAdmin
        
        val name: TableName = TableName.valueOf(namespase + ":" + tableName)
        if(admin.tableExists(name)){
            if(admin.isTableEnabled(name)){
                admin.disableTable(name)
            }
            admin.deleteTable(name)
        }
        
        connection.close()
    }
    
    def addColumnFamilyToTable(tableName: String, familyName: String): Unit = {
        val familyList = getTableColumnFamily(tableName)
        if (familyList.contains(familyName)){
            throw new IllegalArgumentException("columnFamily with this name already exists")
        }
        
        val connection = ConnectionFactory.createConnection(hbaseConfig)
        val admin = connection.getAdmin
        
        val name: String = namespase + ":" + tableName
        admin.addColumnFamily(TableName.valueOf(name),
            ColumnFamilyDescriptorBuilder.of(familyName))
        
        connection.close()
    }
    
    def deleteColumnFamilyFromTable(tableName: String, familyName: String): Unit = {
        val familyList = getTableColumnFamily(tableName)
        if (familyList.size == 1){
            throw new IllegalArgumentException("Family '" + familyName +
                "' is the only column family in the table, so it cannot be deleted")
        }
        if (!familyList.contains(familyName)){
            throw new IllegalArgumentException("columnFamily with this name already not exists")
        }
        
        val connection = ConnectionFactory.createConnection(hbaseConfig)
        val admin = connection.getAdmin
        
        val name: String = namespase + ":" + tableName
        admin.deleteColumnFamily(TableName.valueOf(name),
            Bytes.toBytes(familyName))
        
        connection.close()
    }
    
    def getAllTableNames(): List[String] = {
        val connection = ConnectionFactory.createConnection(hbaseConfig)
        val admin = connection.getAdmin
        
        val tablesList: List[String] = (for (td <- admin.listTableNames()) yield td.getNameAsString).toList
        connection.close()
        tablesList
    }

    def getTableColumnFamily(tableName: String): List[String] = {
        val connection = ConnectionFactory.createConnection(hbaseConfig)
        val admin = connection.getAdmin
        
        val name: String = namespase + ":" + tableName
        val table = admin.listTableDescriptors( List[TableName](TableName.valueOf(name)))(0)
        
        val familyList = (for (cf <- table.getColumnFamilies) yield cf.getNameAsString).toList
        connection.close()
        familyList
    }
    
    def configOurTableInHBase(tableName: String, family: String*): Unit = {
        TableName.isLegalFullyQualifiedTableName(Bytes.toBytes(tableName))
        if(family.size < 1){
            throw new IllegalArgumentException("Table must contain at least one columnFamily")
        }
        val name: TableName = TableName.valueOf(namespase + ":" + tableName)
        val connection = ConnectionFactory.createConnection(hbaseConfig)
        val admin = connection.getAdmin
        
        if(!admin.tableExists(name)){
            createTable(tableName, family(0))
        }
        val familyList = getTableColumnFamily(tableName)
        for (f <- family){
            if (!familyList.contains(f)){
                addColumnFamilyToTable(tableName, f)
            }
        }
        connection.close()
    }
}
