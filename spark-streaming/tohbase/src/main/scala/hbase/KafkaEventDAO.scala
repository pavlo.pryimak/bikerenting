package hbase

import java.time.{LocalDateTime, ZoneId}

import org.apache.hadoop.hbase.TableName
import org.apache.hadoop.hbase.client.{ConnectionFactory, Put}
import org.apache.hadoop.hbase.util.Bytes
import util.Properties

class KafkaEventDAO(val namespace: String, val tableName: String) extends Serializable {
    
    def putKafkaEventToHBase(message: (String,String), familyName: String) ={
        val fullTableName = TableName.valueOf( Bytes.toBytes(namespace + ":" + tableName))
        
        val key: String = message._1 + LocalDateTime.now().atZone(ZoneId.systemDefault()).toInstant().toEpochMilli()
        
        val put = new Put(Bytes.toBytes(key))
    
        put.addColumn(Bytes.toBytes(familyName), Bytes.toBytes("message"), Bytes.toBytes(message._2))
    
        val connection = ConnectionFactory.createConnection(Properties.HBASE_CONFIG)
        val table = connection.getTable(fullTableName )
    
        table.put(put)
    
        table.close()
        connection.close()
    }
    
}
