package hbase

import org.apache.commons.lang.WordUtils.capitalizeFully
import org.apache.hadoop.hbase.client._
import org.apache.hadoop.hbase.util.Bytes
import org.apache.hadoop.hbase.{CellUtil, TableName}
import util.{Properties, StationStatusInfo}

import scala.collection.JavaConverters._

class StreamingDataModelDAO(val namespace: String, val tableName: String) extends Serializable {
    
    def putToTableStationStatus(model: StationStatusInfo, familyName: String): Unit = {
        
        val fullTableName = TableName.valueOf( Bytes.toBytes(namespace + ":" + tableName))
        val put = generatePutToStationStatus(model,familyName)
        
        val connection = ConnectionFactory.createConnection(Properties.HBASE_CONFIG)
        val table = connection.getTable(fullTableName )
        
        table.put(put)
        
        table.close()
        connection.close()
    }
    
    def putBatchToTableStationStatus(models: List[StationStatusInfo], familyName: String): Unit = {
        
        val fullTableName = TableName.valueOf( Bytes.toBytes(namespace + ":" + tableName))
        
        val connection = ConnectionFactory.createConnection(Properties.HBASE_CONFIG)
        val table = connection.getTable(fullTableName )
        
        var batch : java.util.List[Put]  = new java.util.ArrayList[Put]()
        for (model <- models){
            val put = generatePutToStationStatus(model,familyName)
            batch.add(put)
        }
        
        table.put(batch)
        
        table.close()
        connection.close()
    }
    
    def getStreamingDataModelFromStationStatusInRange(familyName: String, startKey: String, stopKey: String): Iterable[StationStatusInfo] = {
        val connection = ConnectionFactory.createConnection(Properties.HBASE_CONFIG)
        val fullTableName = TableName.valueOf( Bytes.toBytes(namespace + ":" + tableName))
        val table = connection.getTable(fullTableName )
        
        val scan = new Scan().
            addFamily(Bytes.toBytes(familyName)).
            withStartRow(Bytes.toBytes(startKey)).
            withStopRow(Bytes.toBytes(stopKey))
        
        val  res = table.getScanner(scan)
        val listStreamingDataModel = res.asScala.map(getStreamingDataModelFromResult)
        res.close()
        
        table.close()
        connection.close()
        
        listStreamingDataModel
    }
    
    def getRowByClassReflection(compositeKey: String, familyName: String, clazz: Class[_]): Map[String, Any] = {
        
        val connection = ConnectionFactory.createConnection(Properties.HBASE_CONFIG)
        val fullTableName = TableName.valueOf( Bytes.toBytes(namespace + ":" + tableName))
        
        val table = connection.getTable(fullTableName )
        var resultMap = Map[String, Any]()
        
        try{
            var get = new Get(Bytes.toBytes(compositeKey))
            get.addFamily(Bytes.toBytes(familyName))
            var result: Result = table.get(get)
            resultMap = getRowReflection(result,familyName,clazz)
        } finally {
            table.close()
            connection.close()
        }
        resultMap
    }
    
    def getStreamingDataModelFromStationStatusByStationId(familyName: String, stationId: Int): Iterable[StationStatusInfo] = {
        val startKey = stationId.toString.concat("-0000000000")
        val stopKey = stationId.toString.concat("-9999999999")
        getStreamingDataModelFromStationStatusInRange(familyName,startKey,stopKey)
    }
    
    private def getRowReflection(result : Result, familyName: String, clazz: Class[_]): Map[String, Any] ={
        var resultMap = Map[String, Any]()
        for (m <- getModelFields(clazz)){
            try {
                val array: Array[Byte] = getValueFromResult(result,familyName,m._1)
                val st = "to" + capitalizeFully(m._2.getSimpleName)
                val method = classOf[Bytes].getMethod(st, classOf[Array[Byte]])
                val data = method.invoke(classOf[Bytes],array)
                resultMap += (m._1 -> data)
            } catch {
                case e: Exception => e.printStackTrace()
            }
        }
        resultMap
    }
    
    private def getStreamingDataModelFromResult(result : Result): StationStatusInfo = {
        val cells = result.rawCells()
        var resultMap = Map[String, Array[Byte]]()
        for(cell <- cells){
            val col_name = Bytes.toString(CellUtil.cloneQualifier(cell))
            val col_value = CellUtil.cloneValue(cell)
            resultMap += (col_name -> col_value)
        }
        StationStatusInfo.createFromMapBytesArray(resultMap)
    }
    
    private def generatePutToStationStatus(model: StationStatusInfo,familyName: String): Put = {
        val compositeKey = StationStatusInfo.getKey(model)
        val put = new Put(Bytes.toBytes(compositeKey))
        
        put.addColumn(Bytes.toBytes(familyName), Bytes.toBytes(StationStatusInfo.tableColumnName(0)), Bytes.toBytes(model.stationId))
        put.addColumn(Bytes.toBytes(familyName), Bytes.toBytes(StationStatusInfo.tableColumnName(1)), Bytes.toBytes(model.numBikesAvailable))
        put.addColumn(Bytes.toBytes(familyName), Bytes.toBytes(StationStatusInfo.tableColumnName(2)), Bytes.toBytes(model.numDocksAvailable))
        put.addColumn(Bytes.toBytes(familyName), Bytes.toBytes(StationStatusInfo.tableColumnName(3)), Bytes.toBytes(model.lastReported))
        put.addColumn(Bytes.toBytes(familyName), Bytes.toBytes(StationStatusInfo.tableColumnName(4)), Bytes.toBytes(model.numEbikesAvailable))
        put.addColumn(Bytes.toBytes(familyName), Bytes.toBytes(StationStatusInfo.tableColumnName(5)), Bytes.toBytes(model.stationStatus))
        
        put
    }
    
    private def getValueFromResult(result: Result, familyName: String, columnName: String): Array[Byte] = {
        result.getValue(Bytes.toBytes(familyName), Bytes.toBytes(columnName))
    }
    
    private def getModelFields(clazz: Class[_]): Map[String,Class[_]] = {
        clazz.getDeclaredFields().map(x => (x.getName, x.getType)).toMap
    }
    
    
}
