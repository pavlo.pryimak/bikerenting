package hbase

import util.StationStatusInfo

trait HBaseWriter extends Serializable {
    def writeStationStatusInfoToHBase(sdm: StationStatusInfo): StationStatusInfo
    def writeKafkaEventToHBase(kafkaMessage: (String,String)): (String,String)
    def flushIfRemain(): Unit
}
