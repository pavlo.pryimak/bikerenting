package streaming

import org.apache.spark.sql.{DataFrame, Dataset, SparkSession}
import streaming.KafkaReportStatus.KafkaReportStatus
import hbase.{HBaseWriter, HBaseWriterEmpty, HBaseWriterImp}
import util.{Properties, StationStatusInfo}
import org.apache.spark.sql.functions.{col, collect_list, window}
import org.apache.spark.sql.streaming.StreamingQuery

import scala.collection.TraversableOnce

object Streaming extends Serializable {
    
    var StateMap = Map[KafkaReportStatus,Map[String,Long]](
        KafkaReportStatus.ZERO_BIKES_AVAILABLE -> Map[String,Long](),
        KafkaReportStatus.ZERO_EBIKES_AVAILABLE -> Map[String,Long](),
        KafkaReportStatus.ZERO_DOCKS_AVAILABLE -> Map[String,Long]()
    )
    
    def main(args: Array[String]): Unit = {
        
        Properties.setProperties(args)
    
        val spark = SparkSession
            .builder()
            .appName("Streaming")
            .master(Properties.MASTER)
            .getOrCreate()

        spark.sparkContext.setLogLevel("ERROR")
        
        val props = Map("schemaRegistryUrl" -> Properties.SCHEMA_REGISTRY_ADDRESS,
            "bootstrapServers" -> Properties.HOSTS_PORTS_TO_READ,
            "topic" -> Properties.TOPIC_TO_READ)
        
        var hbase: HBaseWriter = new HBaseWriterEmpty()
        if (Properties.WRITE_TO_HBASE.equals(true)){
            Properties.configHBase()
            hbase = new HBaseWriterImp(Properties.NAMESPACE,
                Properties.TABLE_NAME_SDM, Properties.FAMILY_NAME_SDM,
                Properties.TABLE_NAME_KE, Properties.FAMILY_NAME_KE)
        }
        
        val consumer : StationInfoConsumer = Properties.INPUT_FORMAT match {
            case "json" => JsonConsumer
            case "avro" => AvroConsumer
            case _ => throw new IllegalArgumentException("Not a valid input format")
        }

        val inputStream = consumer.getStream(spark,props)
        
        val calculation = allCalculation(spark, inputStream,Properties.WINDOW_DURATION, hbase)

        val streamToKafka =  outputKafkaStream(calculation,
            Properties.HOSTS_PORTS_TO_WRITE,
            Properties.TOPIC_TO_WRITE,
            Properties.CHECKPOINT_TO_WRITE)
        
        streamToKafka.awaitTermination()

        hbase.flushIfRemain()
        
        spark.stop()
    }
    
    def allCalculation(spark: SparkSession, data: Dataset[StationStatusInfo], windowDuration: Int, hbase: HBaseWriter): DataFrame={
        import spark.implicits._
        data.
            map(hbase.writeStationStatusInfoToHBase).
            flatMap(streamingDataModelThresholdCheck).
            selectExpr("_1 AS key", "_2 AS value", "cast(_3 as timestamp) as event_time").
            groupBy(window(
                col("event_time"),
                windowDuration + " minutes",
                windowDuration + " minutes") as "window",
                $"key").
            agg(
                collect_list($"value").as("values")
            ).
            select($"key",$"values").
            as[TempModel].
            map(formatKafkaMessage).
            map(hbase.writeKafkaEventToHBase).
            selectExpr("_1 AS key", "_2 AS value").
            selectExpr("CAST(key AS STRING)", "CAST(value AS STRING)")
    }
    
    def outputKafkaStream(dataFrame: DataFrame, bootstrapServers: String, topic: String, checkpoint: String): StreamingQuery ={
        dataFrame.
            writeStream.
            format("kafka").
            outputMode("complete").
            option("kafka.bootstrap.servers", bootstrapServers).
            option("topic", topic).
            option("key.serializer", "org.apache.kafka.common.serialization.StringSerializer").
            option("value.serializer", "org.apache.kafka.common.serialization.StringSerializer").
            option("checkpointLocation", checkpoint)
            .start()
    }
    
    case class TempModel(key: String, values: List[String])
    
    def formatKafkaMessage(tm: TempModel):(String, String) ={
        val stations = tm.values.mkString(", ")
        val message: StringBuilder = new StringBuilder().
            append("At stations ").
            append(stations).
            append("; critically insufficient ").
            append(tm.key)
        (tm.key,message.mkString)
    }
    
    def streamingDataModelThresholdCheck(sdm: StationStatusInfo): TraversableOnce[(String, String, Long)] ={
        var list = List[(String, String, Long)]()
        
        if(sdm.numBikesAvailable <= Properties.MIN_BIKES_THRESHOLD ){
            list = list ::: checkByReportStatus(sdm,KafkaReportStatus.ZERO_BIKES_AVAILABLE)
        }
        if(sdm.numEbikesAvailable <= Properties.MIN_EBIKES_THRESHOLD){
            list = list ::: checkByReportStatus(sdm,KafkaReportStatus.ZERO_EBIKES_AVAILABLE)
        }
        if(sdm.numDocksAvailable <= Properties.MIN_DOCKS_THRESHOLD){
            list = list ::: checkByReportStatus(sdm,KafkaReportStatus.ZERO_DOCKS_AVAILABLE)
        }
        
        def checkByReportStatus(sdm: StationStatusInfo, status: KafkaReportStatus): List[(String, String, Long)] = {
            var mp: Map[String, Long] = StateMap(status)
            var list = List[(String, String, Long)]()
            if(mp.contains(sdm.stationId)){
                if( (sdm.lastReported - mp(sdm.stationId)) > Properties.MESSAGE_DELAY ){
                    mp += (sdm.stationId -> sdm.lastReported)
                    list = list :+ (status.toString, sdm.stationId, sdm.lastReported)
                }
            }else{
                mp += (sdm.stationId -> sdm.lastReported)
                list = list :+ (status.toString, sdm.stationId, sdm.lastReported)
            }
            StateMap += (status -> mp)
            list
        }
        
        list
    }
    

    
}
