from datetime import timedelta, datetime
import airflow
from airflow import DAG
from airflow.contrib.operators.ssh_operator import SSHOperator
from airflow.utils.dates import days_ago
import socket
from airflow.models import Variable


default_args = {
                'owner': 'big-data-poc',
                'depends_on_past': False,
                'start_date': days_ago(0),
             }

#get days schedule
schedule = Variable.get("sqoop_import_schedule_days")

dag = DAG(dag_id='sqoop_import_as_file',
        default_args=default_args,
        schedule_interval=timedelta(days=int(schedule)),
        )

# getting the IP address of postgres
ip_address = socket.gethostbyname('postgres')

#base dir to save import results
destination_dir = Variable.get("sqoop_import_dir")

#get format of file in whitch sqoop import result should be saved
file_format = Variable.get("sqoop_import_format")

pre_import_req = "export HADOOP_USER_NAME=hdfs"

#delete_old_dir = pre_import_req+" && hdfs dfs -rm -r "+destination_dir

#bash commands for tables import
import_weather_bash = pre_import_req+" && sqoop import --username airflow --password airflow --num-mappers 1 --connect jdbc:postgresql://"+ip_address+":5432/airflow --target-dir "+destination_dir+"/weather"+file_format+"--table weather"

import_status_bash = pre_import_req+" && sqoop import --username airflow --password airflow --connect jdbc:postgresql://"+ip_address+":5432/airflow --target-dir "+destination_dir+"/status"+file_format+"--split-by id --table status"

import_station_bash = pre_import_req+" && sqoop import --username airflow --password airflow --connect jdbc:postgresql://"+ip_address+":5432/airflow --target-dir "+destination_dir+"/station"+file_format+"--split-by id --table station"

import_trip_bash = pre_import_req+" && sqoop import --username airflow --password airflow --connect jdbc:postgresql://"+ip_address+":5432/airflow --target-dir "+destination_dir+"/trip "+file_format+" --table trip"

# all the tasks

#delete_old_dir = SSHOperator(
#           ssh_conn_id='sqoop_ssh',
#           task_id='delete_old_dir',
#           command=delete_old_dir,
#           dag=dag)

import_weather_task = SSHOperator(
           ssh_conn_id='sqoop_ssh',
           task_id='import_weather',
           command=import_weather_bash,
           dag=dag)

import_status_task = SSHOperator(
           ssh_conn_id='sqoop_ssh',
           task_id='import_status',
           command=import_status_bash,
           dag=dag)

import_station_task = SSHOperator(
           ssh_conn_id='sqoop_ssh',
           task_id='import_station',
           command=import_station_bash,
           dag=dag)

import_trip_task = SSHOperator(
           ssh_conn_id='sqoop_ssh',
           task_id='import_trip',
           command=import_trip_bash,
           dag=dag)

import_weather_task>>import_trip_task>>import_station_task>>import_status_task
