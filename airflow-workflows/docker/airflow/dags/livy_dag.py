import datetime
import json
import logging
from urllib.parse import urlencode

import requests
from airflow import DAG
from airflow.contrib.sensors.python_sensor import PythonSensor
from airflow.exceptions import AirflowBadRequest, AirflowException
from airflow.hooks.base_hook import BaseHook
from airflow.models import Variable
from airflow.operators.dummy_operator import DummyOperator
from airflow.operators.python_operator import PythonOperator
from airflow.providers.apache.livy.hooks.livy import BatchState

default_args = {
    'owner': 'Yulia Krawtschenko',
    'depends_on_past': False,
    'email': ['Yulia_Krawtschenko@epam.com'],
    'email_on_failure': False,
    'email_on_retry': False,
    'retries': 5,
    'retry_delay': datetime.timedelta(minutes=5),
    'provide_context': True,
}

conn = BaseHook.get_connection('livy_conn_default')

with DAG(
        dag_id='livy_dag',
        default_args=default_args,
        description='Submit Apache Livy batch jobs',
        schedule_interval=Variable.get('livy_schedule'),
        start_date=datetime.datetime.strptime(
            Variable.get('livy_start_date'),
            Variable.get('livy_start_date_format')
        ),
) as livy_dag:
    TERMINAL_STATES = {
        BatchState.SUCCESS,
        BatchState.DEAD,
        BatchState.KILLED,
        BatchState.ERROR,
    }


    def _submit_task(**kwargs):
        req_payload = Variable.get('livy_req_payload', deserialize_json=True)
        req_headers = Variable.get('livy_req_headers', deserialize_json=True)

        res = requests.post(f"{conn.get_uri()}/batches",
                            data=json.dumps(req_payload),
                            headers=req_headers)

        args = req_payload['args'] if 'args' in req_payload.keys() else ''
        conf = req_payload['conf'] if 'conf' in req_payload.keys() else ''

        req_info = f"""
        Submitted Spark job: 
            File: {req_payload['file']}
            Main Class: {req_payload['className']}
            Arguments: {args}
            Configuration: {conf}
            Requested By: {req_headers['X-Requested-By']}
            Poke Interval (s): {Variable.get('livy_poke_interval')}
            Timeout (s): {Variable.get('livy_timeout')}
            Retries: {Variable.get('livy_retries')}
        """
        logging.info(req_info)

        if res.ok:
            batch_id = res.json()['id']
            batch_url = f"{conn.get_uri()}/batches/{batch_id}"

            ti = kwargs['task_instance']
            ti.xcom_push(key="livy_batch_url", value=batch_url)

            res_info = f"""
            Batch ID: {batch_id}
            Batch URL: {batch_url}
            Batch State: {res.json()['state']}
            """
            logging.info(res_info)
            # get_logs()
        else:
            raise AirflowBadRequest(f"Response Status: {res.status_code}\n"
                                    f"Response Text: {res.text}")


    submit_job = PythonOperator(
        task_id='submit_job',
        python_callable=_submit_task,
        retries=int(Variable.get('livy_retries')),
    )


    def _wait_for_completion(**kwargs):

        def print_log(log_json):
            for line in log_json['log']:
                logging.info(line)

        def get_log():
            log_offset = ti.xcom_pull(
                key='livy_log_offset',
                task_ids='wait_for_completion'
            ) or 0
            size = Variable.get('livy_log_size')
            req_params = {"from": log_offset, "size": int(size)}

            log = requests.get(
                f"{batch_url}/log",
                params=urlencode(req_params),
                headers=req_headers
            )

            if log.ok:
                total = int(log.json()['total'])

                ti.xcom_push(
                    key="livy_log_offset",
                    value=total,
                )

                return log
            else:
                raise AirflowException(f"Response Status: {log.status_code}\n"
                                       f"Response Text: {log.text}")

        req_headers = Variable.get('livy_req_headers', deserialize_json=True)

        ti = kwargs['ti']
        batch_url = ti.xcom_pull(key='livy_batch_url', task_ids='submit_job')

        res = requests.get(batch_url, headers=req_headers)
        if res.ok:
            state = BatchState[res.json()['state'].upper()]

            log = get_log()

            if state in TERMINAL_STATES:
                if state == BatchState.SUCCESS:
                    print_log(log.json())

                    return True
                else:
                    raise AirflowException(
                        f"Batch State: {res.json()['state']}\n"
                        f"Log: {log.json()}")
            else:
                print_log(log.json())

                return False
        else:
            raise AirflowException(f"Response Status: {res.status_code}\n"
                                   f"Response Text: {res.text}")


    wait_for_completion = PythonSensor(
        task_id='wait_for_completion',
        python_callable=_wait_for_completion,
        poke_interval=int(Variable.get('livy_poke_interval')),
        timeout=int(Variable.get('livy_timeout')),
        retries=0,
    )

    start = DummyOperator(
        task_id='start',
        dag=livy_dag,
    )

    end = DummyOperator(
        task_id='end',
        dag=livy_dag,
    )

    start >> submit_job >> wait_for_completion >> end

