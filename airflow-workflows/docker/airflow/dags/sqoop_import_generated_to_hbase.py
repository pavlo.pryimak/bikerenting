from datetime import timedelta, datetime
import airflow
from airflow import DAG
from airflow.contrib.operators.ssh_operator import SSHOperator
from airflow.utils.dates import days_ago
import socket
from airflow.models import Variable

default_args = {
                'owner': 'big-data-poc',
                'depends_on_past': False,
                'start_date': days_ago(0),
             }

#get days schedule
schedule = Variable.get("sqoop_import_hbase_schedule_days")

dag = DAG(dag_id='sqoop_import_generated_to_hbase',
        default_args=default_args,
        schedule_interval=timedelta(int(schedule)),
        )

# getting the IP address of postgres
ip_address = socket.gethostbyname('postgres')

#hbase table name
event = "event_imported"

#bash commands for tables import
import_event_bash = "echo \"create '"+event+"', 'station_id'\" | hbase shell -n && sqoop import --username airflow --password airflow --num-mappers 1 --connect jdbc:postgresql://"+ip_address+":5432/airflow --table event --hbase-table "+event+" --column-family station_id  --hbase-row-key station_id "

import_event_task = SSHOperator(
           ssh_conn_id='sqoop_ssh',
           task_id='import_event_to_hbase',
           command=import_event_bash,
           dag=dag)

