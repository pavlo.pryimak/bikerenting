from kaggle import KaggleApi
from io import StringIO
import os
from os import listdir
from os.path import isfile, join
import psycopg2
from airflow import DAG
from datetime import datetime, timedelta
from airflow.operators.python_operator import PythonOperator
from zipfile import ZipFile
from airflow.utils.dates import days_ago
from airflow.models.connection import Connection
from airflow.hooks.base_hook import BaseHook 
import tables_config as conf_sql
import shutil
from airflow.models import Variable

default_args = {
    'owner': 'big-data-poc',
    'depends_on_past': False,
    'start_date': days_ago(0),

}

#connect to postgres
connP = BaseHook.get_connection('bikes_db')

#connect to kaggle api
api = KaggleApi()
api.authenticate()

def start():
    print("Start kaggle import...")

conn = psycopg2.connect(connP.get_uri()) 
cur = conn.cursor()

PATH_TO_STATION_CSV = 'kaggle/station'
PATH_TO_WEATHER_CSV = 'kaggle/weather'
PATH_TO_TRIP_CSV = 'kaggle/trip'
PATH_TO_STATUS_CSV = 'kaggle/status'

#delete kaggle dir

def delete_dir_if_exists():
    dir_path = 'kaggle'
    if os.path.exists(dir_path):
       try:
           shutil.rmtree(dir_path)
       except OSError as e:
           print("Error: %s : %s" % (dir_path, e.strerror))

#dowload cvs using kaggle api

def download_station_cvs():
    api.dataset_download_file('benhamner/sf-bay-area-bike-share', 'station.csv', PATH_TO_STATION_CSV)

def download_weather_cvs():
    api.dataset_download_file('benhamner/sf-bay-area-bike-share', 'weather.csv', PATH_TO_WEATHER_CSV)

def download_trip_cvs():
    api.dataset_download_file('benhamner/sf-bay-area-bike-share', 'trip.csv', PATH_TO_TRIP_CSV)

def download_status_cvs():
    api.dataset_download_file('benhamner/sf-bay-area-bike-share', 'status.csv', PATH_TO_STATUS_CSV)

#create tables

def create_station_table():
    cur.execute(conf_sql.station_drop)
    cur.execute(conf_sql.station_create)
    conn.commit()

def create_trip_table():
    cur.execute(conf_sql.trip_drop)
    cur.execute(conf_sql.trip_create)
    conn.commit()

def create_weather_table():
    cur.execute(conf_sql.weather_drop)
    cur.execute(conf_sql.weather_create)
    conn.commit()


def create_status_table():
    cur.execute(conf_sql.status_drop)
    cur.execute(conf_sql.status_create)
    conn.commit()

#import cvs to postgres

def station_csv_to_postgres():
    station_csv = [f for f in listdir(PATH_TO_STATION_CSV) if isfile(join(PATH_TO_STATION_CSV, f))][0]
    f = open(PATH_TO_STATION_CSV + "/" + station_csv, 'r')
    sql="copy station from stdin DELIMITER ',' CSV HEADER;"
    cur.copy_expert(sql, f)
    conn.commit()


def trip_csv_to_postgres():
    trip_csv_zip = [f for f in listdir(PATH_TO_TRIP_CSV) if isfile(join(PATH_TO_TRIP_CSV, f))][0]
    
    with ZipFile(PATH_TO_TRIP_CSV+'/'+trip_csv_zip, 'r') as zipObj:
         zipObj.extractall(PATH_TO_TRIP_CSV+'/unzipped')
    trip_csv = [f for f in listdir(PATH_TO_TRIP_CSV+'/unzipped') if isfile(join(PATH_TO_TRIP_CSV+'/unzipped', f))][0]
    f = open(PATH_TO_TRIP_CSV + "/unzipped/" + trip_csv, 'r')
    sql="copy trip from stdin DELIMITER ',' CSV HEADER;"
    cur.copy_expert(sql, f)

    conn.commit()

def weather_csv_to_postgres():
    weather_csv = [f for f in listdir(PATH_TO_WEATHER_CSV) if isfile(join(PATH_TO_WEATHER_CSV, f))][0]
    f = open(PATH_TO_WEATHER_CSV + "/" + weather_csv, 'r')
    sql="copy weather from stdin DELIMITER ',' CSV HEADER;"
    cur.copy_expert(sql, f)
    conn.commit()

def status_csv_to_postgres():
    status_csv_zip = [f for f in listdir(PATH_TO_STATUS_CSV) if isfile(join(PATH_TO_STATUS_CSV, f))][0]
    
    with ZipFile(PATH_TO_STATUS_CSV+'/'+status_csv_zip, 'r') as zipObj:
         zipObj.extractall(PATH_TO_STATUS_CSV+'/unzipped')
    status_csv = [f for f in listdir(PATH_TO_STATUS_CSV+'/unzipped') if isfile(join(PATH_TO_STATUS_CSV+'/unzipped', f))][0]
    f = open(PATH_TO_STATUS_CSV + "/unzipped/" + status_csv, 'r')
    sql="copy status from stdin DELIMITER ',' CSV HEADER;"
    cur.copy_expert(sql, f)

    conn.commit()

def close_connection():
    cur.close()
    conn.close()

#get days schedule
schedule = Variable.get("kaggle_import_schedule_days")

with DAG('kaggle_tables_to_postgres', default_args=default_args, schedule_interval=timedelta(days=int(schedule))) as dag:
          start_task = PythonOperator(
              task_id='start',
              python_callable=start
          )

          delete_dir_task =  PythonOperator(
              task_id='delete_dir_if_exists',
              python_callable=delete_dir_if_exists
          )

         # station table operators
          download_station_task = PythonOperator(
              task_id='download_station_cvs',
              python_callable=download_station_cvs
          )

          station_table_task = PythonOperator(
              task_id='create_station_table',
              python_callable=create_station_table
          )

          insert_into_station_task = PythonOperator(
              task_id='insert_values_into_station',
              python_callable=station_csv_to_postgres
            )

	  # trip table operators
          download_trip_task = PythonOperator(
              task_id='download_trip_cvs',
              python_callable=download_trip_cvs
            )

          trip_table_task = PythonOperator(
              task_id='create_trip_table',
              python_callable=create_trip_table
            )

          insert_into_trip_task = PythonOperator(
              task_id='insert_values_into_trip',
              python_callable=trip_csv_to_postgres
            )

          # weather table operators
          download_weather_task = PythonOperator(
              task_id='download_weather_cvs',
              python_callable=download_weather_cvs
            )

          weather_table_task = PythonOperator(
              task_id='create_weather_table',
              python_callable=create_weather_table
            )

          insert_into_weather_task = PythonOperator(
              task_id='insert_values_into_weather',
              python_callable=weather_csv_to_postgres
            )

 	  # status table operators
          download_status_task = PythonOperator(
              task_id='download_status_cvs',
              python_callable=download_status_cvs
            )

          status_table_task = PythonOperator(
              task_id='create_status_table',
              python_callable=create_status_table
            )

          insert_into_status_task = PythonOperator(
              task_id='insert_values_into_status',
              python_callable=status_csv_to_postgres
            )

	  # close connection to postgres
          close_conn_task = PythonOperator(
              task_id='close_conn',
              python_callable=close_connection
            )

	  # dags
          start_task>>delete_dir_task>>download_station_task>>station_table_task>>insert_into_station_task>>close_conn_task
          start_task>>delete_dir_task>>download_trip_task>>trip_table_task>>insert_into_trip_task>>close_conn_task
          start_task>>delete_dir_task>>download_weather_task>>weather_table_task>>insert_into_weather_task>>close_conn_task
          start_task>>delete_dir_task>>download_status_task>>status_table_task>>insert_into_status_task>>close_conn_task



