import time
import random
import psycopg2
from airflow import DAG
from datetime import datetime, timedelta
from airflow.operators.python_operator import PythonOperator
from airflow.hooks.base_hook import BaseHook
from airflow.utils.dates import days_ago

from airflow import DAG
from airflow.models import Variable

MAX_STATION_ID = 15
MAX_EBIKES = 20
MAX_BIKES = 20
POSS_TO_BROKE = 5
station_status = ["active", "out_of_service"]

days_left = 8
SECONDS_IN_A_DAY = 86400
MAX_REPORTS_PER_DAY = 100

current_reports = 0
current_time = int(round((time.time() - (days_left * SECONDS_IN_A_DAY))))

connP = BaseHook.get_connection('bikes_db')

conn = psycopg2.connect(connP.get_uri())

default_args = {
        'owner': 'big_data_poc',
        'depends_on_past': False,
        'start_date': days_ago(0)
        }


class StationInfo:
    def __init__(self, last_reported, station_id, num_ebikes_available,
                 num_bikes_available, num_docks_available, status):
        self.last_reported = last_reported
        self.station_id = station_id
        self.num_ebikes_available = num_ebikes_available
        self.num_bikes_available = num_bikes_available
        self.num_docks_available = num_docks_available
        self.station_status = status


def generate_timestamp(rep, days, time):
    if rep == 0:
        global current_reports
        global days_left
        global current_time
        current_reports = random.randrange(1, MAX_REPORTS_PER_DAY)
        days_left = days - 1
        current_time = time + SECONDS_IN_A_DAY
    else:
        current_reports = rep - 1

    timestamp = random.randrange(time, time + SECONDS_IN_A_DAY)

    return timestamp



def generate_station_status():
    random_numb = random.randrange(0, 100)
    if random_numb > 6:
        return station_status[0]
    else:
        return station_status[1]


def generate_station_info():
    last_reported = generate_timestamp(current_reports, days_left, current_time)
    station_id = random.randrange(1, MAX_STATION_ID)
    num_ebikes_awail = random.randrange(0, MAX_EBIKES)
    num_bikes_awail = random.randrange(0, MAX_BIKES)
    num_docs_awail = random.randrange(0, MAX_BIKES - num_bikes_awail)
    station_status = generate_station_status()

    return StationInfo(last_reported=last_reported, station_id=station_id, num_bikes_available=num_bikes_awail,
                       num_ebikes_available=num_ebikes_awail, num_docks_available=num_docs_awail,
                       status=station_status)


def generate_info(n):
    cur = conn.cursor()
    for i in range(n):
        s = generate_station_info()
        sql = "insert into event (station_id, last_reported, num_ebikes_available, num_bikes_available, num_docks_available, station_status) values ("+str(s.station_id)+", "+str(s.last_reported)+", "+str(s.num_ebikes_available)+", "+str(s.num_bikes_available)+", "+str(s.num_docks_available)+", '"+s.station_status+"')"
        cur.execute(sql)
        conn.commit()

    cur.close()
    conn.close()

def create_table():
    cur = conn.cursor()
    sql_drop = """drop table if exists event"""
    sql_create = """create table if not exists event(
    station_id integer,
    last_reported integer,
    num_ebikes_available integer,
    num_bikes_available integer,
    num_docks_available integer,
    station_status varchar (25) check (station_status in ('active', 'out_of_service'))); """
    cur.execute(sql_drop)
    cur.execute(sql_create)
    conn.commit()

#get days schedule
schedule = Variable.get("sqoop_import_schedule_days")

#get number of records to generate
number_of_records = Variable.get("event_generator_number_of_records")

dag = DAG(dag_id='generate_event_table',
        default_args=default_args,
        schedule_interval=timedelta(days=int(schedule)),
        )

create_table_task = PythonOperator(
        task_id='create_table',
        python_callable= create_table,
        dag=dag
        )

generate_values_task = PythonOperator(
        task_id='generate_values',
        python_callable= generate_info,
        op_kwargs={"n":int(number_of_records)},
        dag=dag
        )

create_table_task>>generate_values_task

