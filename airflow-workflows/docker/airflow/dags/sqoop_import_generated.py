from datetime import timedelta, datetime
import airflow
from airflow import DAG
from airflow.contrib.operators.ssh_operator import SSHOperator
from airflow.utils.dates import days_ago
import socket
from airflow.models import Variable


default_args = {
                'owner': 'big-data-poc',
                'depends_on_past': False,
                'start_date': days_ago(0),
             }

#get days schedule
schedule = Variable.get("sqoop_import_event_schedule_days")

dag = DAG(dag_id='sqoop_import_event_as_file',
        default_args=default_args,
        schedule_interval=timedelta(days=int(schedule)),
        )

# getting the IP address of postgres
ip_address = socket.gethostbyname('postgres')

#base dir to save import results
destination_dir = Variable.get("sqoop_import_dir")

#get format of file in whitch sqoop import result should be saved
file_format = Variable.get("sqoop_import_format")

pre_import_req = "export HADOOP_USER_NAME=hdfs"

#delete_old_dir = pre_import_req+" && hdfs dfs -rm -r "+destination_dir+"/event"

#bash commands for tables import
import_event_bash = pre_import_req+" && sqoop import --username airflow --password airflow --num-mappers 1 --connect jdbc:postgresql://"+ip_address+":5432/airflow --target-dir "+destination_dir+"/event "+file_format+" --table event"


#delete_old_dir = SSHOperator(
#           ssh_conn_id='sqoop_ssh',
#           task_id='delete_old_dir',
#           command=delete_old_dir,
#           dag=dag)

import_event_task = SSHOperator(
           ssh_conn_id='sqoop_ssh',
           task_id='import_event',
           command=import_event_bash,
           dag=dag)

import_event_task
