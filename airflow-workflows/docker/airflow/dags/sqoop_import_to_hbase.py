from datetime import timedelta, datetime
import airflow
from airflow import DAG
from airflow.contrib.operators.ssh_operator import SSHOperator
from airflow.utils.dates import days_ago
import socket
from airflow.models import Variable

default_args = {
                'owner': 'big-data-poc',
                'depends_on_past': False,
                'start_date': days_ago(0),
             }

#get days schedule
schedule = Variable.get("sqoop_import_hbase_schedule_days")

dag = DAG(dag_id='sqoop_import_to_hbase',
        default_args=default_args,
        schedule_interval=timedelta(int(schedule)),
        )

# getting the IP address of postgres
ip_address = socket.gethostbyname('postgres')

#hbase tables names
weather = "weather_imported"
status = "status_imported"
station = "station_imported"
trip = "trip_imported"


#bash commands for tables import
import_weather_bash = "echo \"create '"+weather+"', 'date'\" | hbase shell -n && sqoop import --username airflow --password airflow --num-mappers 1 --connect jdbc:postgresql://"+ip_address+":5432/airflow --table weather --hbase-table "+weather+" --column-family date --hbase-row-key date "

import_status_bash = "echo \"create  '"+status+"', 'time'\" | hbase shell -n  && sqoop import --username airflow --password airflow --num-mappers 2 --split-by id --connect jdbc:postgresql://"+ip_address+":5432/airflow --table status --hbase-table "+status+" --column-family time --hbase-row-key time"

import_station_bash = "echo \"create '"+station+"', 'id'\" | hbase shell -n  && sqoop import --username airflow --password airflow --split-by id --connect jdbc:postgresql://"+ip_address+":5432/airflow --table station --hbase-table "+station+" --column-family id --hbase-row-key id"

import_trip_bash = "echo \"create '"+trip+"', 'id'\" | hbase shell -n  && sqoop import --username airflow --password airflow --num-mappers 2 --connect jdbc:postgresql://"+ip_address+":5432/airflow  --split-by id  --table trip --hbase-table "+trip+" --column-family id --hbase-row-key id"

# all the tasks

import_weather_task = SSHOperator(
           ssh_conn_id='sqoop_ssh',
           task_id='import_weather_to_hbase',
           command=import_weather_bash,
           dag=dag)

import_status_task = SSHOperator(
           ssh_conn_id='sqoop_ssh',
           task_id='import_status_to_hbase',
           command=import_status_bash,
           dag=dag)

import_station_task = SSHOperator(
           ssh_conn_id='sqoop_ssh',
           task_id='import_station_to_hbase',
           command=import_station_bash,
           dag=dag)

import_trip_task = SSHOperator(
           ssh_conn_id='sqoop_ssh',
           task_id='import_trip_to_hbase',
           command=import_trip_bash,
           dag=dag)


import_weather_task>>import_station_task>>import_trip_task>>import_status_task
