#station table

station_drop =  """drop table if exists station"""

station_create =  """ create table station(
    id integer,
    name text,
    lat double precision,
    long double precision,
    dock_count integer,
    city text,
    installation_date date);
    """

#trip table

trip_drop = """drop table if exists trip"""

trip_create =  """ create table trip(
    id integer PRIMARY KEY,
    duration integer,
    start_date timestamptz,
    start_station_name text,
    start_station_id integer,
    end_date timestamptz,
    end_station_name text,
    end_station_id integer,
    bike_id integer,
    subscription_type text,
    zip_code text);
    """

#weather teble

weather_drop = """drop table if exists weather"""

weather_create = """ create table weather(
    date timestamptz,
    max_temperature_f float,
    mean_temperature_f float,
    min_temperature_f float,
    max_dew_point_f float,
    mean_dew_point_f float,
    min_dew_point_f float,
    max_humidity float,
    mean_humidity float,
    min_humidity float,
    max_sea_level_pressure_inches float,
    mean_sea_level_pressure_inches float,
    min_sea_level_pressure_inches float,
    max_visibility_miles float,
    mean_visibility_miles float,
    min_visibility_miles float,
    max_wind_Speed_mph float,
    mean_wind_speed_mph float,
    max_gust_speed_mph float,
    precipitation_inches text,
    cloud_cover float,
    events text default null,
    wind_dir_degrees float,
    zip_code int );
    """

#status table

status_drop =  """drop table if exists status"""

status_create = """ create table status(
    id integer,
    bikes_available integer,
    docks_available integer,
    time timestamp);
    """
