#!/usr/bin/env bash


case "$1" in
  webserver)
#     airflow initdb
     airflow upgradedb
     airflow create_user \
       --role Admin \
       --username airflow} \
       --password airflow}
     airflow connections \
       --add \
       --conn_id bikes_db \
       --conn_type Postgres \
       --conn_login airflow \
       --conn_password airflow \
       --conn_host postgres \
       --conn_port 5432 \
       --conn_schema airflow
     airflow connections \
       --add \
       --conn_id sqoop_ssh \
       --conn_type ssh \
       --conn_login root \
       --conn_password bikes123 \
       --conn_host 10.132.0.23 \
       --conn_port 2222
      # --schema='airflow' \
     airflow connections \
       --add \
       --conn_id livy_conn_default \
       --conn_type http \
       --conn_host 10.132.0.23 \
       --conn_port 8993
     airflow variables \
       -i /livy/default_values.json
   # if [ "$AIRFLOW__CORE__EXECUTOR" = "KubernetesExecutor" ]; then
      # With the "KubernetesExecutor" executors it should all run in one container.
  #    airflow scheduler &
  #  fi
  #  if [ "$AIRFLOW__CORE__EXECUTOR" = "LocalExecutor" ]; then
  #    # With the "Local" executor it should all run in one container.
  #    airflow scheduler &
  #  fi
    exec airflow scheduler &
    exec airflow worker &
    exec airflow webserver
    ;;
  worker|scheduler)
    # To give the webserver time to run initdb.
    sleep 30
    exec airflow "$@"
    ;;
  version)
    exec airflow "$@"
    ;;
  *)
    # The command is something like bash, not an airflow subcommand. Just run it in the right environment.
    exec "$@"
    ;;
esac
