Put all your dags here: /docker/airflow/dags

If you want to specify your own connection in airflow? you can do it in this file:
docker/airflow/entrypoint.sh

Add conn example :
     airflow connections \
       --add \
       --conn_id bikes_db \
       --conn_type Postgres \
       --conn_login airflow \
       --conn_password airflow \
       --conn_host postgres \
       --conn_port 5432 \
       --conn_schema airflow

If you want to add some dependencies to airflow image go to docker/airflow/Dockerfile,
and write them in 'pipDeps' variable

