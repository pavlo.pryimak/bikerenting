package org.example

import java.util.Properties

import io.confluent.kafka.serializers.{AbstractKafkaSchemaSerDeConfig, KafkaAvroSerializer}
import org.apache.kafka.clients.producer.{KafkaProducer, ProducerConfig, ProducerRecord}
import org.apache.kafka.common.errors.SerializationException
import org.apache.kafka.common.serialization.StringSerializer

class AvroProducer
  extends Publisher[StationStatusInfo, String, StationStatusInfo] {

  override var kafkaProducer: KafkaProducer[String, StationStatusInfo] = _
  override var brokers: String = Props.BROKERS
  override var topic: String = Props.TOPIC

  def this(brokers: String, topic: String) {
    this()

    val props = new Properties()
    props.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, brokers)
    props.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, classOf[StringSerializer].getCanonicalName)
    props.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, classOf[KafkaAvroSerializer].getCanonicalName)
    props.put(ProducerConfig.COMPRESSION_TYPE_CONFIG, Props.COMPRESSION_TYPE)
    props.put(AbstractKafkaSchemaSerDeConfig.SCHEMA_REGISTRY_URL_CONFIG, Props.SCHEMA_REGISTRY_URL)
    props.put(AbstractKafkaSchemaSerDeConfig.AUTO_REGISTER_SCHEMAS, true)

    props.put(ProducerConfig.ACKS_CONFIG, "all")
    props.put(ProducerConfig.RETRIES_CONFIG, "10")
    props.put(ProducerConfig.MAX_IN_FLIGHT_REQUESTS_PER_CONNECTION, "5")
    props.put(ProducerConfig.ENABLE_IDEMPOTENCE_CONFIG, "true")

    kafkaProducer = new KafkaProducer[String, StationStatusInfo](props)
  }

  def sendMessage(station: StationStatusInfo): Unit = {
    val record = new ProducerRecord[String, StationStatusInfo](topic, station.stationId, station)

    try {
      kafkaProducer.send(record)
    }
    catch {
      case s: SerializationException => throw s
      case i: InterruptedException => throw i
    }
  }
}
