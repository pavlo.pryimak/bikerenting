package org.example

import org.json4s.ParserUtil.ParseException
import org.json4s.native.Serialization
import org.json4s._

class Json4sJsonParser[T]() extends JsonParser[T] {
  override def parse(json: String, outerElementsKeys: Seq[String])
                    (implicit m: Manifest[T]): T = {
    implicit val formats: Formats = Serialization.formats(NoTypeHints) + new StationStatusInfoJson4sSerializer

    try {
      var parsed = org.json4s.native.JsonMethods.parse(json)
      for (key <- outerElementsKeys) {
        parsed = parsed \ key
      }
      parsed.extract[T]
    }
    catch {
      case p: ParseException => throw new RuntimeException("Could not parse input")
      case e: Exception => throw e
    }
  }
}
