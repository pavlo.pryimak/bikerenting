package org.example

import org.apache.avro.Schema

import scala.io.BufferedSource
import scala.io.Source._

object BikesStatsProducer {
  import scala.util.control.Exception._
  def toLong(str: String): Option[Long] = allCatch.opt(str.toLong)

  def main(args: Array[String]): Unit = {

      // def isSwitch(str: String) = (str.length == 2 && str(0) == '-') || (str(0) == '-' && str(1) == '-')

    args.sliding(2, 2).toList.collect {
      case Array("--brokers", argBrokers: String) => Props.BROKERS = argBrokers
      case Array("--topic", argTopic: String) => Props.TOPIC = argTopic
      case Array("--format", argFormat: String) => Props.OUTPUT_FORMAT = argFormat
      case Array("--compression", argCompression: String) => Props.COMPRESSION_TYPE = argCompression
      case Array("--schema-registry-url", argSchemaRegUrl: String) => Props.SCHEMA_REGISTRY_URL = argSchemaRegUrl
      // case Array("--schema", argSchema: String) => Props.SCHEMA_FILEPATH = argSchema
      case Array("--interval", argInterval: String) => Props.INTERVAL = argInterval.toLong
    }

    val httpGetter: HttpGetter = new SttpHttpGetter
    val jsonParser: JsonParser[Array[StationStatusInfo]] = new Json4sJsonParser[Array[StationStatusInfo]]

    val producer = Props.OUTPUT_FORMAT match {
      case "json" => getJsonProducer
      case "avro" => getAvroProducer
      case _ => throw new IllegalArgumentException("Not a valid output format")
    }

    val driver = Driver(
      producer,
      httpGetter,
      Props.BROKERS,
      Props.TOPIC,
      Props.JSON_URL,
      jsonParser,
      Array(Props.JSON_ROOT_KEY, Props.DATA_ROOT_KEY)
    )

    try {
      driver.run(Props.INTERVAL)
    }
    catch {
      case e: Exception => e.printStackTrace()
    }
  }

  def getJsonProducer: Publisher[StationStatusInfo, String, String] = {
    val jsonWriter = new Json4sJsonWriter[StationStatusInfo]()
    new JsonProducer(Props.BROKERS, Props.TOPIC, jsonWriter)
  }

  def getAvroProducer: Publisher[StationStatusInfo, String, StationStatusInfo] = {
    new AvroProducer(Props.BROKERS, Props.TOPIC)
  }

  def getStationStatusInfoSchema: Schema = {
    var schemaFile: BufferedSource = null
    var schema = ""
    try {
      schemaFile = fromFile(Props.SCHEMA_FILEPATH, "utf-8")
      schema = schemaFile.getLines.mkString
    }
    finally {
      if (schemaFile != null) schemaFile.close
    }
    val schemaParser: Schema.Parser = new Schema.Parser

    schemaParser.parse(schema)
  }
}

    /*

    val timeJsonParser: JsonParser[Long] = new Json4sJsonParser[Long]

    try {
      while (true) {
        var lastLastUpdated = 0L

        var response: String = ""
        try {
          response = httpGetter.get(jsonUrl)
        }
        catch {
          case e: Exception => throw e
        }

        var lastUpdated = 0L
        try {
          lastUpdated = timeJsonParser.parse(response, Array("last_updated"))
        }
        catch {
          case e: Exception => throw e
        }

        if (lastUpdated > lastLastUpdated) {
          lastLastUpdated = lastUpdated

          var parsed: Array[StationStatusInfo] = new Array[StationStatusInfo](0)
          try {
            parsed = jsonParser.parse(response, outerElementsKeys)
          }
          catch {
            case e: Exception => throw e
          }

          parsed.foreach(station => {
            // val stationJson: String = jsonWriter.write(station)
            // producer.send(new ProducerRecord[String, String](topic, key, stationJson))

            val specificAvroWriter: DatumWriter[SpecificRecord] =
              new SpecificDatumWriter[SpecificRecord](StationStatusInfo.getSchema)
            specificAvroWriter.setSchema(StationStatusInfo.getSchema)

            val record = new ProducerRecord[String, StationStatusInfo](topic, station.stationId, station)

            try {
              // producer.send(new ProducerRecord[String, String](topic, key, data))
              producer.send(record)
            }
            catch {
              case s: SerializationException => throw s
              case i: InterruptedException => throw i
            }
          })
        }

 */