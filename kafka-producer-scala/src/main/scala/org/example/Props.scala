package org.example

case object Props {

  val JSON_URL = "https://gbfs.fordgobike.com/gbfs/es/station_status.json"
  val JSON_ROOT_KEY = "data"
  val DATA_ROOT_KEY = "stations"
  val OUTER_ELEMENTS_KEYS: Array[String] = Array(JSON_ROOT_KEY, DATA_ROOT_KEY)

  /*
  var BROKERS = ""
  var TOPIC = ""
  var OUTPUT_FORMAT = ""
  var COMPRESSION_TYPE = ""
  var SCHEMA_REGISTRY_URL = ""
  var SCHEMA_FILEPATH = ""
  var INTERVAL: Long = 1000000

   */

  var BROKERS = "localhost:9092"
  var TOPIC = "bikes"
  var OUTPUT_FORMAT = "avro"
  var COMPRESSION_TYPE = "snappy"
  var SCHEMA_REGISTRY_URL = "http://localhost:8081"
  var SCHEMA_FILEPATH = "src/main/resources/StationStatusInfo.avsc"
  var INTERVAL: Long = 1000
}
