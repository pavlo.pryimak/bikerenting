package org.example

object StationStatus extends Enumeration {
  type StationStatus = Value

  val active,
  out_of_service = Value
}
