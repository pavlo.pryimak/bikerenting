package org.example

trait HttpGetter {
  def get(endpoint: String): String
}