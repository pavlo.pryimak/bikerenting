package org.example

import java.util.Properties

import org.apache.kafka.clients.producer.{KafkaProducer, ProducerConfig, ProducerRecord}
import org.apache.kafka.common.errors.SerializationException
import org.apache.kafka.common.serialization.StringSerializer

class JsonProducer extends Publisher[StationStatusInfo, String, String] {

  override var kafkaProducer: KafkaProducer[String, String] = _
  override var brokers: String = Props.BROKERS
  override var topic: String = Props.TOPIC
  var jsonWriter: JsonWriter[StationStatusInfo] = _

  def this(brokers: String, topic: String) {
    this()
  }

  def this(brokers: String, topic: String, jsonWriter: JsonWriter[StationStatusInfo]) {
    this(brokers, topic)
    this.jsonWriter = jsonWriter

    val props = new Properties()
    props.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, brokers)
    props.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, classOf[StringSerializer].getCanonicalName)
    props.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, classOf[StringSerializer].getCanonicalName)

    props.put(ProducerConfig.ACKS_CONFIG, "all")
    props.put(ProducerConfig.RETRIES_CONFIG, "10")
    props.put(ProducerConfig.MAX_IN_FLIGHT_REQUESTS_PER_CONNECTION, "5")
    props.put(ProducerConfig.ENABLE_IDEMPOTENCE_CONFIG, "true")

    kafkaProducer = new KafkaProducer[String, String](props)
  }

  def sendMessage(station: StationStatusInfo): Unit = {
    val stationJson: String = jsonWriter.write(station)
    try {
      kafkaProducer.send(new ProducerRecord[String, String](topic, station.stationId, stationJson))
    }
    catch {
      case s: SerializationException => throw s
      case i: InterruptedException => throw i
    }
  }
}
