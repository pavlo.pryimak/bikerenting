package org.example

import org.apache.kafka.clients.producer.KafkaProducer

trait Publisher[T, K <: Any, V <: Any] {
  var kafkaProducer: KafkaProducer[K, V]
  var brokers: String
  var topic: String

  def sendMessage(obj: T)
}
