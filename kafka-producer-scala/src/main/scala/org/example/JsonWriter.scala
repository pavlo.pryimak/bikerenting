package org.example

trait JsonWriter[T] {
  def write(obj: T): String
}
