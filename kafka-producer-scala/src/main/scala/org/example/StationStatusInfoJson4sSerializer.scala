package org.example

import StationStatus.StationStatus
import org.json4s.{CustomSerializer, Formats, NoTypeHints}
import org.json4s.JsonAST.{JField, JInt, JLong, JObject, JString}
import org.json4s.JsonDSL._
import org.json4s.ext.{EnumNameSerializer, JavaTypesSerializers}
import org.json4s.native.Serialization

class StationStatusInfoJson4sSerializer extends CustomSerializer[StationStatusInfo] (_ => ( {
  case json: JObject =>
    implicit lazy val formats: Formats = Serialization.formats(NoTypeHints) ++
      JavaTypesSerializers.all + new EnumNameSerializer(StationStatus)

    val stationId: String = (json \ Fields.station_id.toString).extract[String]
    val numBikesAvailable: Int = (json \ Fields.num_bikes_available.toString).extract[Int]
    val numDocksAvailable: Int = (json \ Fields.num_docks_available.toString).extract[Int]
    val lastReported: Long = (json \ Fields.last_reported.toString).extract[Long]
    val numEbikesAvailable: Int = (json \ Fields.num_ebikes_available.toString).extract[Int]
    val stationStatus: StationStatus = (json \ Fields.station_status.toString).extract[StationStatus]

    StationStatusInfo(stationId,
      numBikesAvailable,
      numDocksAvailable,
      lastReported,
      numEbikesAvailable,
      stationStatus
    )
}, {
  case stationStatusInfo: StationStatusInfo => {
    implicit lazy val formats: Formats = Serialization.formats(NoTypeHints) ++
      JavaTypesSerializers.all + new EnumNameSerializer(StationStatus)

    JObject(JField(s"$Fields.station_id", JString(stationStatusInfo.stationId.toString))) ~
      JField(s"$Fields.num_bikes_available", JInt(stationStatusInfo.numBikesAvailable)) ~
      JField(s"$Fields.num_docks_available", JInt(stationStatusInfo.numDocksAvailable)) ~
      JField(s"$Fields.last_reported", JLong(stationStatusInfo.lastReported)) ~
      JField(s"$Fields.num_ebikes_available", JInt(stationStatusInfo.numBikesAvailable)) ~
      JField(s"$Fields.station_status", JString(stationStatusInfo.stationStatus.toString))
  }
}
))


