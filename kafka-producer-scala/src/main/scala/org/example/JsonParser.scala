package org.example

trait JsonParser[T] {
  def parse(json: String, outerElementsKeys: Seq[String])(implicit m: Manifest[T]): T
}
