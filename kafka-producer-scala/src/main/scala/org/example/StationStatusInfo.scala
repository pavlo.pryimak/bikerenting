package org.example

import StationStatus.StationStatus
import org.apache.avro.Schema
import org.apache.avro.specific.{SpecificRecord, SpecificRecordBase}

object StationStatusInfo {
  private val SCHEMA$: Schema = new Schema.Parser().parse("{\"type\":\"record\",\"name\":\"StationStatusInfo\",\"fields\":[{\"name\":\"station_id\",\"type\":[{\"type\":\"string\",\"avro.java.string\":\"String\"},\"null\"]},{\"name\":\"num_bikes_available\",\"type\":[\"int\",\"null\"]},{\"name\":\"num_docks_available\",\"type\":[\"int\",\"null\"]},{\"name\":\"last_reported\",\"type\":[\"long\",\"null\"]},{\"name\":\"num_ebikes_available\",\"type\":[\"int\",\"null\"]},{\"name\":\"station_status\",\"type\":[{\"type\":\"string\",\"avro.java.string\":\"String\"},\"null\"]}]}")

  def getSchema: Schema = SCHEMA$
}

case class StationStatusInfo(stationId: String,
                             numBikesAvailable: Int, // non-negative
                             numDocksAvailable: Int,
                             lastReported: Long,
                             numEbikesAvailable: Int,
                             stationStatus: StationStatus
                            )
  extends SpecificRecordBase
    with SpecificRecord {
  var station_id: String = stationId
  var num_bikes_available: Int = numEbikesAvailable
  var num_docks_available: Int = numDocksAvailable
  var last_reported: Long = lastReported
  var num_ebikes_available: Int = numEbikesAvailable
  var station_status: String = stationStatus.toString

  override def getSchema: Schema = StationStatusInfo.getSchema

  override def get(field$: Int): Any = field$ match {
    // case Fields.station_id.id => this.get("station_id")
    case 0 => station_id
    case 1 => num_bikes_available
    case 2 => num_docks_available
    case 3 => last_reported
    case 4 => num_ebikes_available
    case 5 => station_status
    case _ => throw new IndexOutOfBoundsException("Invalid index: " + field$)
  }

  /*
  override def get(field$: Int): Any = {
    for (field <- Fields.values) {
      if (field.id == field$) this.get(field.toString)
    }
    throw new IndexOutOfBoundsException("Invalid index: " + field$)
  }

   */

  override def put(field$: Int, value$: Any): Unit = {
    field$ match {
      case 0 =>
        station_id = if (value$ != null) value$.toString
        else null

      case 1 =>
        num_bikes_available = value$.asInstanceOf[Integer]

      case 2 =>
        num_docks_available = value$.asInstanceOf[Integer]

      case 3 =>
        last_reported = value$.asInstanceOf[Long]

      case 4 =>
        num_ebikes_available = value$.asInstanceOf[Integer]

      case 5 =>
        station_status = if (value$ != null) value$.toString
        else null

      case _ =>
        throw new IndexOutOfBoundsException("Invalid index: " + field$)
    }
  }
}

object Fields extends Enumeration {
  type StationInfoField = Value

  val station_id,
  num_bikes_available,
  num_docks_available,
  last_reported,
  num_ebikes_available,
  station_status = Value
}