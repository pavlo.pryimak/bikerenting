package org.example

import org.json4s.ext.{EnumNameSerializer, JavaTypesSerializers}
import org.json4s.native.Serialization
import org.json4s.{Formats, NoTypeHints}

class Json4sJsonWriter[T] extends JsonWriter[T] {
  override def write(obj: T): String = {
    implicit lazy val formats: Formats = Serialization.formats(NoTypeHints) ++
      JavaTypesSerializers.all + new EnumNameSerializer(StationStatus)

    org.json4s.native.Serialization.write(obj)
  }
}
