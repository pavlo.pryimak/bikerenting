package org.example

import org.apache.kafka.common.errors.SerializationException

case class Driver[K, V](producer: Publisher[StationStatusInfo, K, V],
                  httpGetter: HttpGetter,
                  brokers: String,
                  topic: String,
                  jsonUrl: String,
                  jsonParser: JsonParser[Array[StationStatusInfo]],
                  outerElementsKeys: Seq[String] = new Array[String](0)) {

  def run(interval: Long): Unit = {
    try {
      while (true) {
        var response: String = ""
        try {
          response = httpGetter.get(jsonUrl)
        }
        catch {
          case e: Exception => throw e
        }

        var parsed: Array[StationStatusInfo] = new Array[StationStatusInfo](0)
        try {
          parsed = jsonParser.parse(response, outerElementsKeys)
        }
        catch {
          case e: Exception => throw e
        }

        parsed.foreach(station => {
          try {
            producer.sendMessage(station)
          }
          catch {
            case s: SerializationException => throw s
            case i: InterruptedException => throw i
          }
        })

        Thread.sleep(interval)
      }
    }

    finally {
      if (producer != null) {
        producer.kafkaProducer.flush()
        producer.kafkaProducer.close()
      }

    }
  }
}
