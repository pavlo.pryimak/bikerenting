package org.example

import sttp.client.SttpClientException.{ConnectException, ReadException}
import sttp.model.StatusCode

class SttpHttpGetter extends HttpGetter {
  import sttp.client._

  override def get(endpoint: String): String = {
    implicit val backend: SttpBackend[Identity, Nothing, NothingT] = HttpURLConnectionBackend()

    try {
      val req = basicRequest.get(uri"$endpoint")
      // .response(asJson[StationInformation]))
      val res = backend.send(req)
      if (res.code != StatusCode.Ok) throw new RuntimeException("Request unsuccessful")

      res.body.right.getOrElse(throw new RuntimeException(res.body.left.get))
    }
    catch {
      case c: ConnectException => throw new RuntimeException("Unable to connect to host")
      case r: ReadException => throw new RuntimeException("No response from host")
      case e: Exception => throw e
    }
    finally {
      backend.close()
    }
  }
}