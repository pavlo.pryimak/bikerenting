Example command to run Scala Kafka producer:

sudo docker run --rm -d --network host yulia/station-status-producer:test --topic testing --brokers localhost:9092 --format avro --compression snappy --schema-registry-url http://localhost:8081 --interval 50000000 

[IMPORTANT] This option was currently removed:
--schema src/main/resources/StationStatusInfo.avsc 