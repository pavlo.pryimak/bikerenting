package com.epam

import java.util.Properties

import org.apache.kafka.clients.producer.{KafkaProducer, ProducerConfig, ProducerRecord}

class Producer(topic: String, broker_host: String) {

  val properties = new Properties()

  properties.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, broker_host)
  properties.put(ProducerConfig.CLIENT_ID_CONFIG, "Bikes station batch analyze producer")
  properties.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, "org.apache.kafka.common.serialization.StringSerializer")
  properties.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, "org.apache.kafka.common.serialization.StringSerializer")

  val producer = new KafkaProducer[String, String](properties)

  def send_message(message: String): Unit = {
    val producer_record = new ProducerRecord[String, String](topic, System.nanoTime().toString, message)
    producer.send(producer_record)

  }

  def close(): Unit = {
    producer.close()
  }


}
