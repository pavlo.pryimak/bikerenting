package com.epam

import java.time.LocalDate
import java.util
import java.util.logging.{Level, Logger}

import org.apache.spark.sql.{DataFrame, SparkSession}
import org.apache.spark.sql.functions.{asc, avg, col, desc, window}


object Main {

  val sparkS = SparkSession.builder
    .appName("[SPARK_BATCH] Top min and max stations on docks available")
    .config("mapreduce.input.fileinputformat.input.dir.recursive", "true")
    .getOrCreate()

  import sparkS.implicits._

  val KAFKA_TAG = "[SPARK_BATCH][TOP_BY_DOCKS_AVAILABLE]"
  val TIME_PERIOD_ARR = Array("YESTERDAY", "WEEK", "MONTH", "YEAR")

  def main(args: Array[String]): Unit = {

    Logger.getLogger("org").setLevel(Level.OFF)
    Logger.getLogger("akka").setLevel(Level.OFF)

    val PATH_TO_FILE = args(0)
    val TOP_ROWS_LIMIT = args(1).toInt

    // TIME_PERIOD can be: YESTERDAY, WEEK, MOUNT, YEAR
    val TIME_PERIOD = args(2)

    val WINDOW_DURATION = args(3)
    val RESULTS_PATH = args(4)
    val KAFKA_TOPIC = args(5)
    val KAFKA_BROKER = args(6)

    // check TIME_PERIOD value
    check_time_period(TIME_PERIOD)

    val df = sparkS
      .read
      .format("com.databricks.spark.avro")
      .load(PATH_TO_FILE)

    // add event time
    val with_event_time = df.selectExpr("*", "cast(last_reported as timestamp) as event_time")

    // calculate average bikes with windowing
    val all_data_windowed = with_event_time.groupBy(window(col("event_time"), WINDOW_DURATION),
      col("station_id")).agg(avg(col("num_docks_available")).as("docks_available"))
      .selectExpr("window.start", "cast(window.start as date) as date", "station_id", "cast(docks_available" +
        " as integer) as docks_available").selectExpr("cast(start as string) as date_time",
      "cast(date as string) as date", "station_id", "docks_available").persist


    val filtered_data = filter_by_time_period(start_date = get_start_date(TIME_PERIOD), all_data_windowed)


    // get top stations with max bikes

    val top_max = filtered_data.orderBy(desc("docks_available")).persist

    // top stations with min bikes
    val top_min = filtered_data.orderBy(asc("docks_available")).persist

    // save to file

    save_as_csv(top_max, RESULTS_PATH + "/top_max")
    save_as_csv(top_min, RESULTS_PATH + "/top_min")

    val kafka_producer = new Producer(topic = KAFKA_TOPIC, broker_host = KAFKA_BROKER)
    // send to kafka
    val top_max_messages = build_message(top_max.limit(TOP_ROWS_LIMIT), "[MAX]",
      "[" + TIME_PERIOD + "]")
    val top_min_messages = build_message(top_min.limit(TOP_ROWS_LIMIT), "[MIN]",
      "[" + TIME_PERIOD + "]")

    send_messages_to_kafka(producer = kafka_producer, messages = top_max_messages)
    send_messages_to_kafka(producer = kafka_producer, messages = top_min_messages)

    kafka_producer.close()
  }

  def build_message(df: DataFrame, top_type_tag: String, time_period_tag: String): util.ArrayList[String] = {

    val messages = new util.ArrayList[String]()

    val dataset = df.selectExpr("date_time", "date", "station_id", "docks_available")
      .as[DocksStatus]

    val list = dataset.map(m => "time: " + m.date_time + ", station_id: " + m.station_id + ", docks_available: " + m.docks_available).collectAsList()

    for (i <- 0 until list.size()) {
      messages.add(KAFKA_TAG + top_type_tag + time_period_tag + "[PLACE " + (i + 1) + "][ON:" + LocalDate.now().toString + "]" + list.get(i) + "\n")
    }

    messages
  }

  def get_start_date(time_period: String): String = {
    val date = LocalDate.now()
    var result_date = ""
    if (time_period.equals("YESTERDAY")) {
      result_date = date.minusDays(2).toString
    }

    if (time_period.equals("WEEK")) {
      result_date = date.minusWeeks(1).minusDays(1).toString
    }

    if (time_period.equals("MONTH")) {
      result_date = date.minusMonths(1).minusDays(1).toString
    }

    if (time_period.equals("YEAR")) {
      result_date = date.minusYears(1).minusDays(1).toString
    }

    result_date
  }

  def filter_by_time_period(start_date: String, df_to_filter: DataFrame): DataFrame = {
    df_to_filter.filter(col("date").gt(start_date))

  }

  def check_time_period(time_period: String): Boolean = {
    if (!TIME_PERIOD_ARR.contains(time_period)) {
      throw new RuntimeException("[Error] Wrong input parameter!!! You must specify time period using one of this values: YESTERDAY, WEEK, MOUNT, YEAR")
    }

    true
  }

  def send_messages_to_kafka(producer: Producer, messages: util.ArrayList[String]): Unit = {
    for (i <- 0 until messages.size()) {
      producer.send_message(messages.get(i))
    }

  }

  def save_as_csv(dataFrame: DataFrame, location: String): Unit = {
    dataFrame
      .coalesce(1)
      .select("date_time", "station_id", "docks_available")
      .write
      .option("header", "true")
      .option("sep", ",")
      .mode("overwrite")
      .csv(location)
  }


}


case class DocksStatus(date_time: String, date: String, station_id: String, docks_available: String)


