package com.epam

import java.time.LocalDate

import com.epam.Main
import org.apache.spark.sql.Row
import org.apache.spark.sql.types.{IntegerType, StringType, StructField, StructType}
import org.scalatest.funsuite.AnyFunSuite


class MainSuite extends AnyFunSuite{

  test("test get_start_date with YESTERDAY"){
    val TIME_PERIOD = "YESTERDAY"
    val result = Main.get_start_date(TIME_PERIOD)

    assert(result.equals("2020-08-25"))
  }

  test("test get_start_date with WEEK"){
    val TIME_PERIOD = "WEEK"
    val result = Main.get_start_date(TIME_PERIOD)

    assert(result.equals("2020-08-19"))
  }

  test("test get_start_date with MONTH"){
    val TIME_PERIOD = "MONTH"
    val result = Main.get_start_date(TIME_PERIOD)


    assert(result.equals("2020-07-26"))
  }

  test("test get_start_date with YEAR"){
    val TIME_PERIOD = "YEAR"
    val result = Main.get_start_date(TIME_PERIOD)


    assert(result.equals("2019-08-26"))
  }

  test("test filter_by_time_period"){

    val data = Seq(
      Row("2020-06-19", "2",3),
      Row("2020-08-18", "2",3),
      Row("2020-08-20", "2",3),
      Row("2020-08-25", "2",3)
    )

    val schema = List(
      StructField("date", StringType, true),
      StructField("station_id", StringType, true),
      StructField("docks_available", IntegerType, true)
    )
    val df = Main.sparkS.createDataFrame(Main.sparkS.sparkContext.parallelize(data), StructType(schema))

    val start_date = Main.get_start_date("WEEK")

    val result_df = Main.filter_by_time_period(start_date, df)

    df.show()

    result_df.show()

    assert(result_df.count()==2)

  }

  test("test build_messages "){
    val data = Seq(
      Row("2020-06-19", "2",3),
      Row("2020-08-18", "2",3),
      Row("2020-08-20", "2",3),
      Row("2020-08-25", "2",3)
    )

    val schema = List(
      StructField("date", StringType, true),
      StructField("station_id", StringType, true),
      StructField("docks_available", IntegerType, true)
    )
    val df = Main.sparkS.createDataFrame(Main.sparkS.sparkContext.parallelize(data), StructType(schema))

    val res = Main.build_message(df, "[MAX]", "[YESTERDAY]")


    assert(res.size()!=0)


  }

  test("test check_time_period"){
    val time_period = "WEwEK"

    assert(Main.check_time_period(time_period))

  }

}
