
Example: 
spark-submit --class com.epam.Main --master yarn --deploy-mode client top-stations-by-docks-1.0-hdfs+kafka-jar-with-dependencies.jar /topics/scala_confluent/ 3 YESTERDAY "10 minutes" /batch/top_results topic_for_top_results sandbox-hdp.hortonworks.com:6667


args: 
1. PATH_TO_FILE
2. TOP_ROWS_LIMIT - how many rows do you need to show in your top
3. TIME_PERIOD - on whitch top will be build; can be YESTERDAY, WEEK, MONTH, YEAR
4. WINDOW_DURATION - the duration of window that will be used for aggregation of average docks 
5. RESULTS_PATH
6. KAFKA_TOPIC 
7. KAFKA_BROKER sandbox-hdp.hortonworks.com:6667