name := "SparkOnK8sReadWriter"

version := "0.1"

scalaVersion := "2.11.1"

autoScalaLibrary := false

val sparkVersion = "2.4.4"

libraryDependencies ++=Seq(

  "org.apache.spark" %% "spark-core" % sparkVersion % "provided",

  "org.apache.spark" %% "spark-sql" % sparkVersion % "provided"

)
