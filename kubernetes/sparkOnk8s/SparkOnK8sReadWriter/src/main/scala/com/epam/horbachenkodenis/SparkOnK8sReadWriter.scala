package com.epam.horbachenkodenis

import java.util.Calendar

import org.apache.spark.sql.{SaveMode, SparkSession}

object SparkOnK8sReadWriter {

  case class HelloWorld(message: String)

  def main(args: Array[String]): Unit = {

//    val hdfs_master = args(0)
    val hdfs_master = if (args.length > 0) args(0) else "hdfs://nn1home:8020/"
//    val master_url = args(1)

    val sparkSession = SparkSession.builder()
      .appName("SparkOnK8sReadWriter")
//      .master("local[*]")
      .getOrCreate()
    import sparkSession.implicits._

    println("===== Start app... =====")
    val time = Calendar.getInstance().getTime
    println("===== Creating a dataframe with 1 partition... =====")
    val df = Seq(HelloWorld("helloworld " + time)).toDF().coalesce(1)
    println("===== Writing files... =====")
//        df.show()
    df.write.mode(SaveMode.Overwrite).csv(hdfs_master + "user/hdfs/wiki/testwiki.csv")
    println("===== Exiting app... =====")
  }
}
