#!/bin/bash
helm install --name hadoop $(stable/hadoop/tools/calc_resources.sh 50) --set yarn.nodeManager.replicas=0 --set yarn.resourceManager.pdbMinAvailable=0 --set yarn.nodeManager.pdbMinAvailable=0  --set hdfs.dataNode.replicas=2 stable/hadoop  --set nodeSelector."node-role\\.kubernetes\\.io/worker"=
